// Default data (located in data folder):
//===============================================================
// var persons = [
//     { id: 0, name: "John Doe", phone: "+380662906604" },
//     { id: 1, name: "Fox Mulder", phone: "+380662229627" },
//     { id: 2, name: "Duncan Mcleod", phone: "+380662906604" },
//     { id: 3, name: "Dana Scully", phone: "+380662906604" }
// ];
// var holidays = [
//     { id: 0, name: "New Year" },
//     { id: 1, name: "St. Valentine's Day" },
//     { id: 2, name: "Knowledge Day" }
// ];
// var people = [
//     { id: 0, name: "colleague" },
//     { id: 1, name: "friend" },
//     { id: 2, name: "sibling" },
//     { id: 3, name: "parent" }
// ];
// var wishes = [
//     { id: 0, text: "success" },
//     { id: 1, text: "good luck" },
//     { id: 2, text: "all the best" },
//     { id: 3, text: "good health" }
// ];
//===============================================================

//current holyday's Id
var holidayId = 0;


//function for generating random wishes
function generateWishes(personId, holidayId, peopleId) {
    var wishId = Math.floor(Math.random() * 4);
    return "Dear " + persons[personId].name + "! You are good " + people[peopleId].name + " and I want to greet you with " + holidays[holidayId].name +
        " and wish you a " + wishes[wishId].text + "!";
}


//holidays dropdown list handler
function changeHoliday(item) {
    holidayId = $(item).attr("index");
    console.log(holidayId);
    $("#drop").text(holidays[holidayId].name);
}


//function to display and send greetings
function sendGreetings() {

    //Send SMS to my number only - proogf of concept
    var wish = generateWishes(1, holidayId, Math.floor(Math.random() * 4));
    var sms = { message: wish, phone: persons[1].phone };
    $.ajax({
        type: "POST",
        url: "https://sms-greet.azurewebsites.net/api/sms",
        data: JSON.stringify(sms),
        contentType: "application/json; charset=utf-8",
        success: function (data, status) {
            Console.log('SMS sent! Status: OK');
            $('li .just').html('<i class="far fa-check-circle"></i>');
        }
    });


    for (i = 0; i < 4; i++) {
        var wish = generateWishes(i, holidayId, Math.floor(Math.random() * 4));
        var listitem = $('<li class="list-group-item"><span class="just"></span>&nbsp;&nbsp;' + wish + '</li>');
        $("#messages").append(listitem);
        $(".just").html('<i class="fas fa-circle-notch fa-spin"></i>');

        // SMS sending - if you have enough money on the Twilio account
        //===================================================================

        // var sms = { message: wish, phone: persons[i].phone };
        // $.ajax({
        //     type: "POST",
        //     url: "https://sms-greet.azurewebsites.net/api/sms",
        //     data: JSON.stringify(sms),
        //     contentType: "application/json; charset=utf-8",
        //     success: function (data, status) {
        //         Console.log('SMS sent! Status: OK');
        //         $('li .just').html('<i class="far fa-check-circle"></i>');
        //     }
        // });


        //just for test - delay by 1.5s before write checkmarks. Comment this lines for production usage!
        var delayInMilliseconds = 1500;
        setTimeout(function () {
            $('li .just').html('<i class="far fa-check-circle"></i>');
        }, delayInMilliseconds);

    }

}

//function for creating persons table
function personsTable() {
    $('#personTable').DataTable({
        data: persons,
        "columns": [
            { "data": "name", "title": "Name" },
            { "data": "phone", "title": "Phone" }
        ],
        paging: false
    });
}


//function to fill Table modal with data
function createTableInModal(tableType) {
    // tableType can be persons, holidays, people, wishes

    $('#table_container').html(makeTable(tableType));
    $(".json_table").addClass("table table-borderless table-hover");
    $("#save_Table").off("click");

    switch (tableType) {
        case persons:
            $('#exampleModalLabel').html('Peoples table');
            $("#save_Table").click(function () {
                persons = makeJson(); console.log(persons); $('#personTable').dataTable().fnDestroy(); personsTable();
            });
            break;
        case holidays:
            $('#exampleModalLabel').html('Holidays table');
            $("#save_Table").click(function () {
                holidays = makeJson(); console.log(holidays); $("#holidays").empty(); fillHolydaysDropDown();
            });
            break;
        case people:
            $('#exampleModalLabel').html('Roles table');
            $("#save_Table").click(function () {
                people = makeJson(); console.log(people);
            });
            break;
        case wishes:
            $('#exampleModalLabel').html('Wishes table');
            $("#save_Table").click(function () {
                wishes = makeJson(); console.log(wishes);
            });
            break;
    }

}

//fuction to fill Holidays dropdpown with data
function fillHolydaysDropDown() {
    holidays.forEach(function (item, index) {
        $("#holidays").append("<a class='dropdown-item' role='presentation' href='#' index=" + item.id + " onclick='changeHoliday(this)'>" + item.name + "</a>");
    });
}

//main function
$(document).ready(function () {

    //create persons table
    personsTable();

    //fill table modal with default (persons) data and apply a styles
    $('#table_container').html(makeTable(persons));
    $(".json_table").addClass("table table-borderless table-hover");

    //fill holidays dropdown with data
    fillHolydaysDropDown();

    //activate all popovers in document
    $('[data-toggle="popover"]').popover({ html: true });
});


//P.S. It's a good idea to use https://textbelt.com/ instead of Twilio for future releases?!